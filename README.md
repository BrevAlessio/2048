# Implementation of 2048

This is just an implementation of the notorious 2048 game. This implementation uses Canvas for the rendering.

## Install
Clone the repo
`git clone https://gitlab.com/BrevAlessio/2048.git`

Install dependencies
`npm i`

Run the application locally
`npm run serve`