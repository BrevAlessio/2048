const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')


module.exports = {
    mode: "development",
    entry: __dirname + "/src/index.js",
    plugins: [
        new HtmlWebpackPlugin({
            template: __dirname + '/src/views/index.html'
        }),
        new CleanWebpackPlugin(),
    ],
    output: {
        path: __dirname + '/dist',
        filename: "[name].js"
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: [
                            "env"
                        ]
                    }
                }
            },
            {
                test: /\.scss$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "sass-loader"
                ]
            }
        ]
    },
    watch: true
}