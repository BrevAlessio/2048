import ctx from '../Ctx/'
import step from'./step'
import addBlock from'./addBlock'
import move from './move'
import calcPoints from './calcPoints'

class Game {
    constructor () {
        this.points = 0
        this.ctx = ctx
        this.grid = new Array(4).fill(0).map(e => new Array(4).fill(0))
    }

    reset () {
        this.points = 0
        this.grid.length = 0
        this.grid = new Array(4).fill(0).map(e => new Array(4).fill(0))
        return this
    }

    start () {
        this.addBlock()
        return this
    }

    isGameOver () {
        return !this.grid.some(row => row.some(cell => cell === 0))
    }

}

Game.prototype.step = step
Game.prototype.addBlock = addBlock
Game.prototype.move = move
Game.prototype.calcPoints = calcPoints

export default Game