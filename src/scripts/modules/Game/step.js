import drawGrid from '../Drawers/drawGrid'

export default function (keycode) {

    if (!this.move(keycode)) { return false }
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height)
    drawGrid(this.grid, this.ctx.canvas.width, this.ctx)
    this.addBlock()
    
    setTimeout( _ => {
        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height)
        drawGrid(this.grid, this.ctx.canvas.width, this.ctx)
    }, 100 )
        
}