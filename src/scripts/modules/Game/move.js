import traverseRow from '../Helpers/traverseRow'
import reverseGrid from '../Helpers/reverseGrid'
import canMove from '../Helpers/canMove'

export default function (direction) {

    switch (direction) {
        case 38:
            if( !canMove(reverseGrid(this.grid), false) ) { return false}
            this.grid = reverseGrid(traverseRow(reverseGrid(this.grid), 'reduce'))
            break;
        case 39:
            if( !canMove(this.grid, true) ) { return false}
            this.grid = traverseRow(this.grid, 'reduceRight')
            break;
        case 40:
            if( !canMove(reverseGrid(this.grid), true) ) { return false}
            this.grid = reverseGrid(traverseRow(reverseGrid(this.grid), 'reduceRight'))
            break;
        case 37:
            if( !canMove(this.grid, false) ) { return false}
            this.grid = traverseRow(this.grid, 'reduce')
            break;
    }
    return true
}