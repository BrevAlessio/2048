import getAvaiableCells from '../Helpers/getAvaiableCells'
import getRandomInt from '../Helpers/getRandomInt'

export default function () {
    const cells = getAvaiableCells(this.grid)
    const cell = cells[getRandomInt(0, cells.length -1)]
    this.grid[cell[0]][cell[1]] = Math.random() > 0.9? 4 : 2
}