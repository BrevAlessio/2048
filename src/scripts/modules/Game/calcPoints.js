export default function () {
    return this.grid.reduce((acc, row) =>
        acc + row.reduce((rowPoints, cell) => rowPoints + cell, 0)
    , 0)
}