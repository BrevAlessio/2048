export default function (e, game, points) {

    if ([38, 39, 40, 37].includes(e.keyCode)){
        e.preventDefault()
        game.step(e.keyCode)
        return true
    }
    return false
}