export default (grid) => 
    grid.reduce((cells, row, idx) =>
        cells.concat(row.reduce((cells, cell, cellId) =>
            cell === 0? cells.concat([[idx, cellId]]) : cells
        , []))
    , [])