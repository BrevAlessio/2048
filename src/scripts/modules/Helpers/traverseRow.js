export default (grid, reduceMethod) => {

    const increment = reduceMethod === 'reduce'? 1 : -1
    
    grid.forEach((row, idx, arr) => {
      const newRow = row.filter(cell => cell !== 0)[reduceMethod]((acc, cell, idx, row) => {
        if (cell === row[idx + increment]){
            acc.push(cell*2)
            row[idx + increment] = 0
        } else if (cell !== 0){
            acc.push(cell)
        }
        return acc
      }, [])

      const filler = new Array(4 - newRow.length).fill(0)
      arr[idx] = reduceMethod === 'reduce'? newRow.concat(filler) : filler.concat(newRow.reverse())
      arr[idx].length = 4
    })
    return grid
}
