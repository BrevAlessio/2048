export default (grid, reverse) => {
    return grid.some(row => {
        return (reverse? row.slice().reverse() : row).some(
            (cell, idx, row) =>
                (cell !== 0 && cell === row[idx + 1])  || (cell === 0 && !!row[idx + 1])
        )
    });
}