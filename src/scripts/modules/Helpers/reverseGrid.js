export default grid => new Array(4).fill(0).map((col, colId) => grid.map( row => row[colId] ))
