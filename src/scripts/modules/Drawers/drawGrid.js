import variables from '../../../scss/variables.scss'

export default (grid, edge, ctx) => {
    const offset = 10
    const tileSize = (edge / 4) - offset - offset/4
    grid.forEach((row, y) => {
        ctx.fillStyle = variables.dividerColor
        ctx.fillRect(0, 0, edge, edge)

        requestAnimationFrame( _ => 
            row.forEach((cell, x) => {
                if (cell === 0) {
                    ctx.fillStyle = variables.tileEmpty
                    ctx.fillRect(tileSize * x + (x + 1) * offset, tileSize * y + (y + 1) * offset, tileSize , tileSize)
                } else {
                    // Block
                    ctx.fillStyle = variables['tile' + cell]
                    ctx.fillRect(tileSize * x + (x + 1) * offset, tileSize * y + (y + 1) * offset, tileSize , tileSize)
    
                    // Text
                    ctx.textAlign = 'center'
                    ctx.textBaseline = 'middle'
                    ctx.font = variables.fontLargeSize + ' ' + variables.fontFamily
                    ctx.fillStyle = variables.textColor
                    ctx.fillText(cell, tileSize * x + (x + 1) * offset + tileSize/2, tileSize * y + (y + 1) * offset + tileSize/2 )
                }
            })        
        )
    })
}