const Canvas = () => {
    const canvas = document.getElementById('board')
    canvas.width = 500
    canvas.height = 500
    return canvas
}

export default Canvas