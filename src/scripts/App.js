import Game from './modules/Game/'
import drawGrid from './modules/Drawers/drawGrid';
import inputManager from './modules/Helpers/inputManager';

const game = new Game()
const points = document.querySelector('.points__counter')
const gameOver = document.querySelector('.game-over')

document.addEventListener('keydown', e => {
    if (game.isGameOver())
        return gameOver.classList.toggle('game-over--active', true)
    inputManager(e, game, points)
    points.textContent = game.calcPoints()
})

points.textContent = game.start().calcPoints()
setTimeout(_ => drawGrid(game.grid, game.ctx.canvas.width, game.ctx), 200)

document.querySelectorAll('.new-game').forEach(e => e.addEventListener('click', _ => {
        points.textContent = game.reset().start().calcPoints()
        gameOver.classList.toggle('game-over--active', false)
        setTimeout(_ => drawGrid(game.grid, game.ctx.canvas.width, game.ctx), 200)        
    })
)